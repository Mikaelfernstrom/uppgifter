import java.io.IOException;
import java.util.Scanner;
import java.io.PrintWriter;

	public class Lotto {

		public static void main(String[] args) throws IOException{
		
			PrintWriter output = new PrintWriter("Lotto.txt");
			Scanner input = new Scanner(System.in);
			
			int temp;
			
			System.out.println("Enter 7 numbers between 1-35");
			int Lotto[] = new int[7];
			
			for(int i = 0; i < 7; i++){
				temp = input.nextInt();
				if(temp >= 1 && temp <= 35){
					Lotto[i] = temp;
					
				}
				else{
					System.out.println("Only numbers between 1-35");
				i--;
				}
				
			}
			
			for(int i = 0; i < Lotto.length; i++){
				output.print(Lotto[i] + " ");
				}
		output.close();
		}
		
}
