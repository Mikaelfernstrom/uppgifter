import java.util.Scanner;

public class WhileLoop {

	public static void main(String[] args) {
		
		System.out.print("Skriv ett nr, 1-10!\n");
		
		Scanner user_input = new Scanner(System.in);
		
		
		int input = user_input.nextInt();
		int x = 1;
		while(input > 10){
			input = user_input.nextInt();
		}
		while(x <= input){
			System.out.print(x + "\n");
			x++;
		    }
		
		}

	}
