import java.io.IOException;
import java.util.Scanner;
import java.io.PrintWriter;

public class WriteFile {
	public static void main(String[] args) throws IOException{
		
		PrintWriter output = new PrintWriter("Information.txt");
		Scanner user_input = new Scanner(System.in);
		
		String f_name[] = new String[200];
		String s_name[] = new String[200];
		int m_nr[] = new int[200];
		int count = 0;
	
		for(int i = 0; i<=199; i++){
			System.out.println("Whats your first name?");
			f_name[i] = user_input.next();
			if(f_name[i].equals("stop"))
				break;
			System.out.println("Whats your surname?");
			s_name[i] = user_input.next();
	
			System.out.println("Whats your mobilenumber?");
			while(!user_input.hasNextInt()){
				System.out.println("Sorry, only numbers.");
				user_input.next();
			}
			m_nr[i] = user_input.nextInt();
			count++;
			}
			for(int i = 0; i < count; i++){
				output.println(f_name[i] + "\t" + s_name[i] + "\t" + m_nr[i]);
			}
			output.println("stop");
			output.close();
	}
}
